data "aws_iam_policy_document" "allow_automation_doc_exec_from_cloudwatch_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role" "execute_run_runbook_from_cloudwatch_role" {
  name = "AWSCloudWatchRole"

  assume_role_policy = data.aws_iam_policy_document.allow_automation_doc_exec_from_cloudwatch_document.json
}

data "aws_iam_policy_document" "execute_automation_document" {
  statement {
    effect = "Allow"

    actions = [
      "ssm:StartAutomationExecution"
    ]
    resources = ["arn:aws:ssm:${data.aws_region.current.name}:${local.account_id}:automation-definition/${aws_ssm_document.automation_document.name}:$DEFAULT"]
  }
}

resource "aws_iam_role_policy" "start_ssm_automation_policy" {
  name = "ExecuteAutomationDocument"
  role = aws_iam_role.execute_run_runbook_from_cloudwatch_role.id

  policy = data.aws_iam_policy_document.execute_automation_document.json
}

data "aws_iam_policy_document" "pass_role_to_automation_role_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "iam:PassRole"
    ]
    resources = ["${aws_iam_role.ssm_automation_role.arn}"]
  }
}

resource "aws_iam_role_policy" "pass_role_policy" {
  name = "Pass-Role-To-Automation-Role"
  role = aws_iam_role.execute_run_runbook_from_cloudwatch_role.id

  policy = data.aws_iam_policy_document.pass_role_to_automation_role_policy_document.json
}