output "health_check_period" {
  description = "Time after instance comes into service before checking health."
  value       = aws_autoscaling_group.runners_asg.health_check_grace_period
}