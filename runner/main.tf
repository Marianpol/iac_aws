terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.66.0"
    }
  }
  backend "s3" {
    bucket = "marianpol-iac"
    key    = "gitlab-runner/actual.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

locals {
  account_id = data.aws_caller_identity.current.account_id
}

data "aws_ami" "redhat" {
  most_recent = true
  owners      = ["309956199498"]

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "name"
    values = ["RHEL-8*"]
  }
}

resource "aws_security_group" "ssh_security_group" {
  name        = "AllowSSH"
  description = "Allow public SSH traffic"

  ingress {
    description = "Allow inbound SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "http_security_group" {
  name        = "AllowPublicHTTP"
  description = "Allow public traffic on HTTP"

  ingress {
    description = "Allow inbound HTTP traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound HTTP traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "https_security_group" {
  name        = "AllowPublicHTTPS"
  description = "Allow public traffic on HTTPS"

  ingress {
    description = "Allow inbound HTTPS traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outband HTTPS traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "runner-ssh-key"
  public_key = base64decode(var.ssh_public_key)
}

resource "aws_launch_template" "runner_template" {
  name                   = "runner_template"
  instance_type          = "m6i.xlarge"
  update_default_version = true
  image_id               = data.aws_ami.redhat.id
  vpc_security_group_ids = [
    aws_security_group.ssh_security_group.id,
    aws_security_group.http_security_group.id,
    aws_security_group.https_security_group.id
  ]
  key_name = aws_key_pair.ssh_key.key_name
  user_data = base64encode(templatefile("${path.module}/init-config.sh",
    {
      registration_token    = var.runner_registration_key,
      aws_access_key_id     = var.aws_access_key_id,
      aws_secret_access_key = var.aws_secret_access_key,
      aws_default_region    = data.aws_region.current.name
  }))

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "GitlabRunner"
    }
  }
}

resource "aws_autoscaling_lifecycle_hook" "on_terminate_hook" {
  name                   = "on_terminate_hook"
  autoscaling_group_name = aws_autoscaling_group.runners_asg.name
  default_result         = "CONTINUE"
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_group" "runners_asg" {
  availability_zones = ["eu-central-1a"]
  name               = "gitlab-runners-asg"
  desired_capacity   = 1
  min_size           = 1
  max_size           = 2

  mixed_instances_policy {
    instances_distribution {
      spot_max_price           = 0.15
      spot_allocation_strategy = "lowest-price"
    }

    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.runner_template.id
        version            = "$Default"
      }
    }
  }
}

resource "aws_ssm_document" "automation_document" {
  name            = "Automation-Document"
  document_type   = "Automation"
  document_format = "YAML"

  content = file("${path.module}/ec2-instance-termination-doc.yaml")
}

resource "aws_cloudwatch_event_rule" "termination_event_pattern" {
  name        = "Capture-Termination-Event"
  description = "Capture EC2 termination event"

  event_pattern = templatefile("${path.module}/cloudwatch-event-pattern.json", { asg_name = aws_autoscaling_group.runners_asg.name })
}

resource "aws_cloudwatch_event_target" "ec2_termination_event_target" {
  target_id = "AutomationDocument"
  rule      = aws_cloudwatch_event_rule.termination_event_pattern.name
  arn       = "arn:aws:ssm:${data.aws_region.current.name}:${local.account_id}:automation-definition/${aws_ssm_document.automation_document.name}:$DEFAULT"
  role_arn  = aws_iam_role.execute_run_runbook_from_cloudwatch_role.arn

  input_transformer {
    input_paths = {
      asgname    = "$.detail.AutoScalingGroupName",
      instanceid = "$.detail.EC2InstanceId",
      lchname    = "$.detail.LifeCycleHookName",
    }
    input_template = <<EOF
{
  "InstanceId": <instanceid>,
  "ASGName": <asgname>,
  "LCHName": <lchname>,
  "AutomationAssumeRole": ${jsonencode(aws_iam_role.ssm_automation_role.arn)}
}
EOF
  }
}