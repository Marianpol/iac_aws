#!/bin/bash
dnf install -y git unzip
yum install -y yum-utils

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
aws configure set aws_access_key_id ${aws_access_key_id}
aws configure set aws_secret_access_key ${aws_secret_access_key}
aws configure set default.region ${aws_default_region}

su - ec2-user -c "mkdir ~/.gitlab-runner"
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
rpm -i gitlab-runner_amd64.rpm
gitlab-runner register --non-interactive \
--url "https://gitlab.com/" \
--registration-token ${registration_token} \
--executor shell --description "rhel-runner" \
--tag-list "aws,rhel" \
--run-untagged="true" \
--locked="true"

cp -R /root/.aws /home/gitlab-runner/
chown -R gitlab-runner /home/gitlab-runner/.aws

groupadd docker
usermod -aG docker gitlab-runner
newgrp docker

su - ec2-user -c "sudo cp /etc/gitlab-runner/config.toml ~/.gitlab-runner/"
su - ec2-user -c "sudo chown ec2-user ~/.gitlab-runner/config.toml"