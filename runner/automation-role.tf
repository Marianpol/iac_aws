data "aws_iam_policy_document" "ssm_automation_role_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role" "ssm_automation_role" {
  name = "AWSAutomationRole"

  assume_role_policy = data.aws_iam_policy_document.ssm_automation_role_document.json
}

data "aws_iam_policy_document" "allow_completelifecycle_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "autoscaling:CompleteLifecycleAction"
    ]
    resources = ["${aws_autoscaling_group.runners_asg.arn}"]
  }
}

resource "aws_iam_role_policy" "allow_completelifecycle_policy" {
  name = "Allow-CompleteLifecycle"
  role = aws_iam_role.ssm_automation_role.id

  policy = data.aws_iam_policy_document.allow_completelifecycle_policy_document.json
}

data "aws_iam_policy_document" "ssm_automation_actions_policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "ssm:DescribeInstanceInformation",
      "ssm:ListCommands",
      "ssm:ListCommandInvocations"
    ]
    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:SendCommand"
    ]
    resources = ["arn:aws:ssm:${data.aws_region.current.name}::document/AWS-RunShellScript"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:SendCommand"
    ]
    resources = ["arn:aws:ec2:*:*:instance/*"]
  }
}

resource "aws_iam_role_policy" "ssm_automation_actions_policy" {
  name = "SSM-Automation-Actions"
  role = aws_iam_role.ssm_automation_role.id

  policy = data.aws_iam_policy_document.ssm_automation_actions_policy_document.json
}