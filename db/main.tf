terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.66.0"
    }
  }
  backend "s3" {
    bucket = "marianpol-iac"
    key    = "db/actual.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
}

data "aws_region" "current" {}

resource "aws_db_instance" "postgresql" {
  name                   = "prismadb"
  engine                 = "postgres"
  engine_version         = "13.3"
  instance_class         = "db.t3.micro"
  username               = "postgres"
  password               = "postgres"
  storage_type           = "gp2"
  allocated_storage      = 20
  multi_az               = false
  publicly_accessible    = true
  vpc_security_group_ids = [aws_security_group.allow_postgresql.id]
  availability_zone      = "${data.aws_region.current.name}a"
  skip_final_snapshot    = true
}

resource "aws_security_group" "allow_postgresql" {
  name        = "postgresql"
  description = "Allow two-direction traffic for PostgreSQL"

  ingress {
    description = "Allow inbound DB traffic"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound DB traffic"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
