output "db_endpoint" {
    description = "PostgreSQL endpoint"
    value       = format("%s%s/%s", 
        "postgresql://",  
        aws_db_instance.postgresql.endpoint, 
        aws_db_instance.postgresql.name)
}