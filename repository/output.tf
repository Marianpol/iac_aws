output "backend_url" {
  description = "URL of newly created container images registry"
  value       = aws_ecr_repository.backend.repository_url
}