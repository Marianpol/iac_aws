terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.66.0"
    }
  }
  backend "s3" {
    bucket = "marianpol-iac"
    key    = "repository/actual.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
}

data "aws_caller_identity" "current" {}

locals {
    account_id = data.aws_caller_identity.current.account_id
}

resource "aws_ecr_repository" "backend" {
  name = "nest-backend"

  image_scanning_configuration {
    scan_on_push = false
  }
}

data "aws_iam_policy_document" "ecr_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.account_id}:user/Gitlab"]
    }
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:PutImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart"
    ]
  }
}

resource "aws_ecr_repository_policy" "backend_policy" {
  repository = aws_ecr_repository.backend.name

  policy = data.aws_iam_policy_document.ecr_policy_document.json
}